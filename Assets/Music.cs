﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Music : MonoBehaviour {

    public Button musicStop;
    public AudioSource musicPlayer;

    // Use this for initialization
	void Start () {
        musicStop.onClick.AddListener(() =>
        {
            if(musicPlayer.volume == 0f)
            {
                musicPlayer.volume = 0.036f;
                musicStop.image.sprite = Resources.Load<Sprite>("Sprite/sound_on");
            }
            else
            {
                musicPlayer.volume = 0f;
                musicStop.image.sprite = Resources.Load<Sprite>("Sprite/sound_off");
            }
        });
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
