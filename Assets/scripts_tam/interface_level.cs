﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class interface_level : MonoBehaviour {

    private static Text main_counter;
    public GameObject main_counter_gameobject;

    public static void draw_main_counter()
    {
        long total_counter = 0;
        foreach(Region r in Level.regions)
        {
            total_counter += r.getOwnDominance();
        }

        main_counter.text = total_counter.ToString("n0") + " cm²";
    }

	// Use this for initialization
	void Start () {
        main_counter = main_counter_gameobject.GetComponent<Text>();
	}
}
