﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ignore_transparency : MonoBehaviour {

    private void Awake()
    {
        Image image = GetComponent<Image>();
        image.alphaHitTestMinimumThreshold = 0.1f;

        Button btn = GetComponent<Button>();
        Navigation customNav = new Navigation();
        customNav.mode = Navigation.Mode.None;
        btn.navigation = customNav;
    }
}
