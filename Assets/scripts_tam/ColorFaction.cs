﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorFaction{

    public string nom;
    public Color color;

    public ColorFaction(string nom, Color color)
    {
        this.nom = nom;
        this.color = color;
    }
}
