﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class interface_region : MonoBehaviour {


    public GameObject fenetre;
    private static GameObject fenetre_static;
    private static GameObject panel_region;
    private static Text nom_region;
    private static Text info_region;

    private static Region region_shown;

    private static interface_region this_interface_region;

    public static bool showing_info;

    private static void hide()
    {
        showing_info = false;
        panel_region.SetActive(false);
    }

    private static void show(Region region)
    {
        showing_info = true;

        region_shown = region;

        nom_region.text = region.getNom();

        info_region.text = getInfo(region);

        panel_region.SetActive(true);

        this_interface_region.StartCoroutine(this_interface_region.wait_to_hide());
    }

    public static void onClick(Region region)
    {
        if (!showing_info && !fenetre_static.activeSelf)
        {
            show(region);
        }
    }

    private static string getInfo(Region region)
    {
        Dictionary<ColorFaction, float> partition = region.getPartition();
        string info = "";
        foreach (KeyValuePair<ColorFaction, float> pair in partition)
        {
            info += ("<b><color=#" + ColorUtility.ToHtmlStringRGBA(pair.Key.color) + "> " + (Mathf.Round(pair.Value * 1000000) / 10000) + "%</color></b>\n");
        }

        bool open = false;

        if (info != "")
        {
            open = true;
            info += "<size=18>\nof " + region.getMaximumTerritory().ToString("n0") + " cm²\n\n";
        }

        if (region.getOwnDominance() > 0)
        {
            if (!open)
            {
                open = true;
                info += "<size=18>";
            }

            info += "you have " + region.getOwnPainter().ToString("n0") + " painters\n";

            if (region.getOwnPopularity() != 0)
            {
                info += "and a popularity of " + (region.getOwnPopularity() < 0 ? "-" : "+") + Mathf.Abs(region.getOwnPopularity()) + "%";
            }
        }
        else
        {
            if (!open)
            {
                open = true;
                info += "<size=18>";
            }

            info += "you have no paint there yet";
        }
        
        if (open)
        {
            info += "</size>";
        }
        

        /*Dictionary<ColorFaction, long> painters = region.getPainters();
        foreach (KeyValuePair<ColorFaction, long> pair in painters)
        {
            info += (pair.Key.nom + " : " + pair.Value.ToString() + " painters\n");
        }*/

        /*Dictionary<ColorFaction, float> popularities = region.getPopularities();
        foreach (KeyValuePair<ColorFaction, float> pair in popularities)
        {
            info += (pair.Key.nom + " : " + pair.Value.ToString() + " popularity\n");
        }*/

        /*LinkedList<KeyValuePair<ColorFaction, float>> drawn_partition = region.getDrawnPartition();
        LinkedListNode<KeyValuePair<ColorFaction, float>> iterator = drawn_partition.First;
        while(iterator != null)
        {
            info += (iterator.Value.Key.nom + " : " + (Mathf.Round(iterator.Value.Value * 1000) / 10).ToString() + "%\n");
            iterator = iterator.Next;
        }*/

        return info;
    }

    // Use this for initialization
    void Start () {
        this_interface_region = this;

        fenetre_static = fenetre;

        panel_region = transform.Find("Game").Find("panel_region").gameObject;
        nom_region = panel_region.transform.Find("nom_region").GetComponent<Text>();
        info_region = panel_region.transform.Find("info_region").GetComponent<Text>();

        hide();
	}

    IEnumerator wait_to_hide()
    {
        yield return new WaitUntil(() => Input.GetMouseButtonUp(0) || Input.GetKey(KeyCode.Escape));
        yield return new WaitUntil(() => Input.GetMouseButtonDown(0) || Input.GetKey(KeyCode.Escape));
        yield return new WaitUntil(() => Input.GetMouseButtonUp(0) || Input.GetKey(KeyCode.Escape));
        hide();
    }
	
	// Update is called once per frame
	void Update () {
		if(showing_info)
        {
            //update info
            info_region.text = getInfo(region_shown);
        }
	}
}