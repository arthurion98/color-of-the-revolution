﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Region {

    /*Variables*/
    private string nom;

    private long max_territory; //territory covered by the region
    private long max_painters;
    private Dictionary<ColorFaction, long> dominance; //each color can range from 0 to max_territory
    private Dictionary<ColorFaction, long> painters;
    private Dictionary<ColorFaction, float> popularities; //each color popularity from -100 to 100

    /*Methods*/
    public Region(string nom, long max_territory)
    {
        this.nom = nom;
        this.max_territory = max_territory;
        max_painters = max_territory;
        dominance = new Dictionary<ColorFaction, long>();
        painters = new Dictionary<ColorFaction, long>();
        popularities = new Dictionary<ColorFaction, float>();
    }

    public void onClick()
    {
        interface_region.onClick(this);
    }

    public string getNom()
    {
        return nom;
    }

    public long getTotaldominance()
    {
        long total_dominance = 0;
        foreach (long value in dominance.Values)
        {
            total_dominance += value;
        }
        return total_dominance;
    }

    public long getMaximumTerritory()
    {
        return max_territory;
    }

    public void newColor(ColorFaction newColor_)
    {
        dominance[newColor_] = 0;
        painters[newColor_] = 0;
        popularities[newColor_] = 0;
    }

    public void addColor(ColorFaction color_, long number)
    {
        long total_dominance = 0;
        foreach (long value in dominance.Values)
        {
            total_dominance += value;
        }
        
        dominance[color_] = Clamp(dominance[color_] + number, 0, max_territory);

        if (number > 0 && number > max_territory - total_dominance) //increasing and no free-territory left -> eat
        {
            List<ColorFaction> colors = getColors();

            long eat = number - (max_territory - total_dominance);
            int other_color = colors.Count - 1;
            if (other_color > 0) //else nothing to eat
            {
                long slice_eat = eat / other_color;
                long remainder = 0;

                //dividing without taking rounding errors into account
                foreach(ColorFaction c in colors)
                {
                    if (c != color_)
                    {
                        remainder = Max(0, slice_eat - dominance[c]);
                        addColor(c, -slice_eat);

                        eat -= (slice_eat - remainder);
                        if (other_color > 1)
                        {
                            other_color--;
                            slice_eat = eat / other_color;
                        }
                        else
                        {
                            break;
                        }
                    }
                }

                //ensuring the total is max_territory by trying to find a color where to put the difference
                foreach(ColorFaction c in colors)
                {
                    if (c != color_)
                    {
                        total_dominance = 0;
                        foreach (long value in dominance.Values)
                        {
                            total_dominance += value;
                        }

                        addColor(c, -Max(0, max_territory - total_dominance));
                    }
                }
            }
        }
    }

    public void addColors(long number)
    {
        List<ColorFaction> k = getAllColors();

        foreach (ColorFaction c in k)
        {
            addColor(c, number);
        }
    }

    public void multiplyColor(ColorFaction color_, long multiplier)
    {
        addColor(color_, (dominance[color_] * (multiplier - 1)));
    }

    public void divideColor(ColorFaction color_, long denominator)
    {
        addColor(color_, ((dominance[color_] * (1 - denominator))/denominator));
    }

    public List<ColorFaction> getColors() //return the colors that have power in the region
    {
        List<ColorFaction> colors = new List<ColorFaction>();
        foreach (ColorFaction color in dominance.Keys)
        {
            if(dominance[color] > 0)
            {
                colors.Add(color);
            }
        }

        return colors;
    }

    public void multiplyColors(long multiplier)
    {
        List<ColorFaction> k = getAllColors();

        foreach (ColorFaction c in k)
        {
            multiplyColor(c, multiplier);
        }
    }

    public void divideColors(long denominator)
    {
        List<ColorFaction> k = getAllColors();

        foreach (ColorFaction c in k)
        {
            multiplyColor(c, denominator);
        }
    }

    public List<ColorFaction> getAllColors()
    {
        List<ColorFaction> colors = new List<ColorFaction>();
        foreach (ColorFaction color in dominance.Keys)
        {
            colors.Add(color);
        }

        return colors;
    }

    public void resetColor(ColorFaction color_)
    {
        dominance[color_] = 0;
    }

    public void resetColors()
    {
        List<ColorFaction> k = getColors();

        foreach(ColorFaction c in k)
        {
            resetColor(c);
        }
    }

    public Dictionary<ColorFaction, float> getPartition() //return the territory partition for the colors in the region (sum of all parts is equal to 1); partition only if all territory reached
    {
        float total_dominance = 0;
        foreach (float value in dominance.Values)
        {
            total_dominance += value;
        }

        Dictionary<ColorFaction, float> partition = new Dictionary<ColorFaction, float>();
        if (total_dominance != 0)
        {
            foreach (ColorFaction color in dominance.Keys)
            {
                if (total_dominance > max_territory)
                {
                    partition[color] = (1.0f*dominance[color]) / total_dominance;
                }
                else
                {
                    partition[color] = (1.0f*dominance[color]) / max_territory;
                }
            }
        }

        return partition;
    }

    public LinkedList<KeyValuePair<ColorFaction, float>> getDrawnPartition() //allow to draw the partition, for example if 1/3, 1/3, 1/3, then the circle will be 1, 2/3, 1/3 (summing the precedent); ordered from lowest to largest
    {
        List<ColorFaction> colors = getAllColors();
        Dictionary<ColorFaction, float> partition = getPartition();

        LinkedList<KeyValuePair<ColorFaction, float>> drawnPartition = new LinkedList<KeyValuePair<ColorFaction, float>>();

        if (partition.Count > 0)
        {
            float previous_min_tot = 0;

            while (colors.Count > 0)
            {
                float min = 2; //init over the maximum
                ColorFaction min_color = colors[0]; //init with the first
                foreach (ColorFaction color in colors)
                {
                    if (partition[color] < min)
                    {
                        min = partition[color];
                        min_color = color;
                    }
                }

                previous_min_tot += min;
                drawnPartition.AddLast(new KeyValuePair<ColorFaction, float>(min_color, Mathf.Clamp01(previous_min_tot))); //added rounding security with Clamp01

                colors.Remove(min_color);
            }
        }

        return drawnPartition;
    }

    public long getOwnDominance()
    {
        return dominance[Level.ownColor];
    }

    public long getOwnPainter()
    {
        return painters[Level.ownColor];
    }

    public float getOwnPopularity()
    {
        return popularities[Level.ownColor];
    }

    public void addPainter(ColorFaction color_, long painter_number)
    {
        painters[color_] = Clamp(painters[color_] + painter_number, 0, max_painters);
    }

    public void addPainters(long painter_number)
    {
        List<ColorFaction> k = getAllColors();

        foreach (ColorFaction c in k)
        {
            addPainter(c, painter_number);
        }
    }

    public void multiplyPainter(ColorFaction color_, long multiplier)
    {
        addPainter(color_, (painters[color_] * (multiplier - 1)));
    }

    public void dividePainter(ColorFaction color_, long denominator)
    {
        addPainter(color_, ((painters[color_] * (1 - denominator)) / denominator));
    }

    public void multiplyPainters(long multiplier)
    {
        List<ColorFaction> k = getAllColors();

        foreach (ColorFaction c in k)
        {
            multiplyPainter(c,multiplier);
        }
    }

    public void dividePainters(long denominator)
    {
        List<ColorFaction> k = getAllColors();

        foreach (ColorFaction c in k)
        {
            dividePainter(c, denominator);
        }
    }

    public void resetPainter(ColorFaction color_)
    {
        painters[color_] = 0;
    }

    public void resetPainters()
    {
        List<ColorFaction> k = getAllColors();

        foreach (ColorFaction c in k)
        {
            resetPainter(c);
        }
    }

    public Dictionary<ColorFaction, long> getPainters()
    {
        return painters;
    }

    public Dictionary<ColorFaction, float> getPopularities()
    {
        return popularities;
    }

    public void addPopularity(ColorFaction color_, float number)
    {
        popularities[color_] = Mathf.Clamp(popularities[color_] + number, -100, 100);
    }

    public void addPopularities(float number)
    {
        List<ColorFaction> k = getAllColors();

        foreach (ColorFaction c in k)
        {
            addPopularity(c, number);
        }
    }

    public void multiplyPopularity(ColorFaction color_, float multiplier)
    {
        addPopularity(color_, popularities[color_] * (multiplier - 1));
    }

    public void multiplyPopularities(float multiplier)
    {
        List<ColorFaction> k = getAllColors();

        foreach (ColorFaction c in k)
        {
            multiplyPopularity(c, multiplier);
        }
    }

    public void resetPopularity(ColorFaction color_)
    {
        popularities[color_] = 0;
    }

    public void resetPopularities()
    {
        List<ColorFaction> k = getAllColors();

        foreach (ColorFaction c in k)
        {
            resetPopularity(c);
        }
    }

    public void update()
    {
        painters_draw();
        painters_update();
    }

    public void painters_draw()
    {
        List<ColorFaction> k = getAllColors();

        //all other colors
        foreach (ColorFaction c in k)
        {
            if (c != Level.ownColor)
            {
                addColor(c, painters[c] * Level.sizeDrawer); //each painter paint 1*size cm2
            }
        }
        //your color drawn last (advantage)
        addColor(Level.ownColor, painters[Level.ownColor] * Level.sizeDrawer);
    }

    public void painters_update()
    {
        List<ColorFaction> k = getAllColors();

        foreach (ColorFaction c in k)
        {
            if (popularities[c] != 0)
            {
                int painter_to_add = Mathf.RoundToInt(Random.Range(0f, (Mathf.Abs(popularities[c]) / 100)) * painters[c]);
                
                if (popularities[c] < 0)
                {
                    painter_to_add = -painter_to_add;
                }
                
                addPainter(c, painter_to_add);
            }
        }
    }

    private static long Clamp(long value, long min, long max)
    {
        if(value < min)
        {
            value = min;
        }
        else if(value > max)
        {
            value = max;
        }
        return value;
    }

    private static long Max(long a, long b)
    {
        if (a > b)
        {
            return a;
        }
        else
        {
            return b;
        }
    }
}
