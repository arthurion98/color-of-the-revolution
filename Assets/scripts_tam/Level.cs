﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level : MonoBehaviour {

    public Initializer initializer;
    public static Initializer initializer_static;

    public static List<Region> regions;
    public static List<ColorFaction> colors;
    public static ColorFaction ownColor;
    public static ColorFaction color2, color3;
    public static List<Event> events = new List<Event>();

    public EventManager eventManager;
    public PinceauButton pinceauButton;

    public static List<region_drawer> drawers;

    public static int sizePinceau = 1;
    public static int sizeDrawer = 100;

    public static float chrono = 0;
    public static float cycle_time = 0.2f;

    public static float chronoEvent = 0;
    public static float cycle_time_event = 2f;

    public static float chronoEventDelete = 0;
    public static float cycle_time_delete = 5f;

    long max_territory = 1480000000000000000;

    public static bool inGame = false;

    public static void newColor(ColorFaction newColor_)
    {
        foreach(Region r in regions)
        {
            r.newColor(newColor_);
        }

        foreach(region_drawer d in drawers)
        {
            d.newColor(newColor_);
        }
    }

    public void draw()
    {
        foreach(region_drawer d in drawers)
        {
            d.draw();
        }
    }

    public void update_region()
    {
        foreach(Region r in regions)
        {
            r.update();
        }
    }

    public long getTotalDominanceAllRegion()
    {
        long total = 0;
        foreach(Region r in regions)
        {
            total += r.getTotaldominance();
        }
        return total;
    }

    public void update()
    {
        update_region();
        draw();
        interface_level.draw_main_counter();
    }

    public void deleteEvent()
    {
        EventManager.removeEvent();
        //Debug.Log(getTotalDominanceAllRegion());
        foreach(Region r in regions)
        {
            r.divideColor(ownColor, 2);
            r.dividePainter(ownColor, 2);
        }
        //Debug.Log(getTotalDominanceAllRegion());
    }

    public void chooseEvent()
    {
        long actualDominance = getTotalDominanceAllRegion(); //Permet de situer à peu près où en est le joueur dans la partie
        float ratio = actualDominance == 0 ? 0 : (float)actualDominance / max_territory; //donne un ratio sur 1 du territoire déjà conquis par tous les joueurs

        //Debug.Log(ratio);
        int numberOfEvent = 1;
        if (ratio < 0.00000000001f) //Début de jeu, temps normal et seulement 1 event toutes les deux sec
        {
            cycle_time_delete = 5f;
            cycle_time_event = 2f;
        }
        else if (ratio >= 0.00000000001f && ratio < 0.001f) //cadence accélérée et potentiellement deux events
        {
            cycle_time_event = 1.5f;
            cycle_time_delete = 4f;
            numberOfEvent = Random.Range(1, 5) == 1 ? numberOfEvent + 1 : numberOfEvent;
        }
        else //cadence maximum, et beaucoup de chance d'avoir trois events à la fois
        {
            cycle_time_event = 1f;
            cycle_time_delete = 3f;
            numberOfEvent = Random.Range(1, 5) == 1 ? numberOfEvent + 1 : numberOfEvent;
            numberOfEvent = Random.Range(1, 3) == 1 ? numberOfEvent + 1 : numberOfEvent;
        }

        if (actualDominance < max_territory / 1000000000000) //event de début de jeu
        {
            //à choisir de façon aléatoire parmis les event de début de jeu
            for (int i = 0; i < numberOfEvent; ++i)
            {
                EventManager.addEvent(events[Random.Range(0, 37)]);
            }
        }
        else
        {
            //à choisir de façon aléatoire parmis les autres event
            for (int i = 0; i < numberOfEvent; ++i)
            {
                EventManager.addEvent(events[Random.Range(0, 52)]);
            }
        }

        if(EventManager.eventList.Count > 99) //si on dépasse 99, on réduit jusqu'à 99
        {
            for(int i = EventManager.eventList.Count; i > 99; --i)
            {
                deleteEvent();
            }
        }
    }

    public void clock()
    {
        chrono += Time.deltaTime;
        chronoEvent += Time.deltaTime;
        if(EventManager.eventList.Count != 0)
        {
            chronoEventDelete += Time.deltaTime;
        }
        else
        {
            chronoEventDelete = 0;
        }

        if(chrono > cycle_time)
        {
            chrono = 0;
            update();
        }
        if(chronoEvent > cycle_time_event)
        {
            chronoEvent = 0;
            chooseEvent();
        }
        if(chronoEventDelete > cycle_time_delete)
        {
            //Debug.Log("message perdu");
            chronoEventDelete = 0;
            deleteEvent();
        }
    }

    public static Region randomRegion()
    {
        return regions[Random.Range(0, regions.Count)];
    }

    public void addColorRegions(ColorFaction color_, long number)
    {
        foreach(Region r in regions)
        {
            r.addColor(color_, number);
        }
    }

    public void multiplyColorRegions(ColorFaction color_, long multiplier)
    {
        foreach(Region r in regions)
        {
            r.multiplyColor(color_, multiplier);
        }
    }

    public void divideColorRegions(ColorFaction color_, long denominator)
    {
        foreach (Region r in regions)
        {
            r.divideColor(color_, denominator);
        }
    }

    public void addColorsRegions(long number)
    {
        foreach (Region r in regions)
        {
            r.addColors(number);
        }
    }

    public void multiplyColorsRegions(ColorFaction color_, long multiplier)
    {
        foreach (Region r in regions)
        {
            r.multiplyColors(multiplier);
        }
    }

    public void divideColorsRegions(ColorFaction color_, long denominator)
    {
        foreach (Region r in regions)
        {
            r.divideColors(denominator);
        }
    }

    public void addPainterRegions(ColorFaction color_, long number)
    {
        foreach (Region r in regions)
        {
            r.addPainter(color_, number);
        }
    }

    public void multiplyPainterRegions(ColorFaction color_, long multiplier)
    {
        foreach (Region r in regions)
        {
            r.multiplyPainter(color_, multiplier);
        }
    }

    public void dividePainterRegions(ColorFaction color_, long denominator)
    {
        foreach (Region r in regions)
        {
            r.dividePainter(color_, denominator);
        }
    }

    public void addPaintersRegions(long number)
    {
        foreach (Region r in regions)
        {
            r.addPainters(number);
        }
    }

    public void multiplyPaintersRegions(ColorFaction color_, long multiplier)
    {
        foreach (Region r in regions)
        {
            r.multiplyPainters(multiplier);
        }
    }

    public void dividePaintersRegions(ColorFaction color_, long denominator)
    {
        foreach (Region r in regions)
        {
            r.dividePainters(denominator);
        }
    }

    public void addPopularityRegions(ColorFaction color_, float number)
    {
        foreach (Region r in regions)
        {
            r.addPopularity(color_, number);
        }
    }

    public void multiplyPopularityRegions(ColorFaction color_, float multiplier)
    {
        foreach (Region r in regions)
        {
            r.multiplyPopularity(color_, multiplier);
        }
    }

    public void addPopularitiesRegions(float number)
    {
        foreach (Region r in regions)
        {
            r.addPopularities(number);
        }
    }

    public void multiplyPopularitiesRegions(ColorFaction color_, float multiplier)
    {
        foreach (Region r in regions)
        {
            r.multiplyPopularities(multiplier);
        }
    }

    // Use this for initialization
    void Start () {
        regions = new List<Region>();
        colors = new List<ColorFaction>();
        drawers = new List<region_drawer>();

        initializer_static = initializer;
	}
	
	// Update is called once per frame
	void Update () {

        if(inGame) clock();

        if(inGame)
        {
            if (Input.GetKeyDown(KeyCode.A))
            {
                eventManager.openNewEvent();
            }

            if (Input.GetKeyDown(KeyCode.Space))
            {
                pinceauButton.draw();
            }
        }
    }
}
