﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class region_drawer : MonoBehaviour {

    public GameObject slice_prefab;

    private Dictionary<ColorFaction,Image> slices;
    private Region assigned_region;
    private Vector3 center;


    public void initialize(Region assigned_region, Vector3 center)
    {
        this.assigned_region = assigned_region;
        this.center = center;
        slices = new Dictionary<ColorFaction, Image>();
    }

    public void newColor(ColorFaction color)
    {
        GameObject slice = GameObject.Instantiate(slice_prefab, transform);
        slice.transform.localPosition = center;
        slices[color] = slice.GetComponent<Image>();
        slices[color].color = color.color;
        slices[color].fillAmount = 0;
    }

    public void draw()
    {
        LinkedList<KeyValuePair<ColorFaction, float>> drawn_partition = assigned_region.getDrawnPartition();

        if (drawn_partition.Count > 0)
        {
            LinkedListNode<KeyValuePair<ColorFaction, float>> iterator = drawn_partition.First;
            int order = drawn_partition.Count - 1;
            while (iterator != null)
            {
                slices[iterator.Value.Key].transform.SetSiblingIndex(order);
                slices[iterator.Value.Key].fillAmount = iterator.Value.Value;
                iterator = iterator.Next;
                order--;
            }
        }
        else //clear
        {
            foreach(Image slice in slices.Values)
            {
                slice.fillAmount = 0;
            }
        }
    }

    // Use this for initialization
    void Start () {

	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
