﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Effet {

    /* Variables */                

    /*Constructor*/
    public Effet()
    {
       
    }

    /* Méthodes */
    public virtual void affecter() { }
}
