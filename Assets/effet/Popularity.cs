﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Popularity : Effet {

    int popularity;
    Region region;
    ColorFaction faction;
    bool reset;
    bool randomRegion;
    bool multiplication;
    bool allRegion;

    public Popularity(int popularity, ColorFaction faction, Region region, bool reset = false, bool randomRegion = false, bool multiplication = false, bool allRegion = false) : base()
    {
        this.popularity = popularity;
        this.region = region;
        this.faction = faction;
        this.reset = reset;
        this.randomRegion = randomRegion;
        this.multiplication = multiplication;
        this.allRegion = allRegion;
    }

    public override void affecter()
    {
        base.affecter();

        if (randomRegion) region = Level.randomRegion();
        List<Region> regionConcerned = new List<Region> { region };
        if (allRegion) regionConcerned = Level.regions;

        foreach(Region r in regionConcerned)
        {
            if (!reset)
            {
                if (multiplication)
                {
                    r.multiplyPopularity(faction, popularity);
                }
                else
                {
                    r.addPopularity(faction, popularity);
                }
            }
            else
            {
                r.resetPopularity(faction);
            }
        }
        
    }
}

