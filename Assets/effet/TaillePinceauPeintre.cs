﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TaillePinceauPeintre : Effet {

    int size;
    bool reset;
    bool multiplication;

	public TaillePinceauPeintre(int size, bool reset = false, bool multiplication = false) : base()
    {
        this.size = size;
        this.reset = reset;
        this.multiplication = multiplication;
    }

    public override void affecter()
    {
        base.affecter();

        if (!reset)
        {
            if (multiplication)
            {
                Level.sizeDrawer = Level.sizeDrawer * size;
            }
            else
            {
                Level.sizeDrawer = Mathf.Max(1,Level.sizeDrawer + size);
            }
        }
        else
        {
            Level.sizeDrawer = 200;
        }
    }
}
