﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Peintres : Effet {

    int number;
    ColorFaction faction;
    Region region;
    bool reset;
    bool randomRegion;
    bool multiplication;
    bool allRegion;

    public Peintres(int number, ColorFaction color, Region region, bool reset = false, bool randomRegion = false, bool multiplication = false, bool allRegion = false) : base()
    {
        this.number = number;
        this.region = region;
        this.reset = reset;
        this.faction = color;
        this.randomRegion = randomRegion;
        this.multiplication = multiplication;
        this.allRegion = allRegion;
    }

    public override void affecter()
    {
        base.affecter();

        if (randomRegion) region = Level.randomRegion();
        List<Region> regionConcerned = new List<Region> { region };
        if (allRegion) regionConcerned = Level.regions;

        foreach(Region r in regionConcerned)
        {
            if (!reset)
            {
                if (multiplication)
                {
                    r.multiplyPainter(faction, number);
                }
                else
                {
                    r.addPainter(faction, number);
                }
            }
            else
            {
                r.resetPainter(faction);
            }
        }
        
    }
}
