﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerEvent : Effet {

    Event e;

    public TriggerEvent(Event e) : base()
    {
        this.e = e;
    }

    public override void affecter()
    {
        base.affecter();

        //On ajoute l'event à la liste d'event globale
        EventManager.addEvent(e);
    }
}
