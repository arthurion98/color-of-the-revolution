﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TaillePinceau : Effet {

    int size;
    bool reset;
    bool multiplication;

    public TaillePinceau(int size, bool reset = false, bool multiplication = false) : base()
    {
        this.size = size;
        this.reset = reset;
        this.multiplication = multiplication;
    }

    public override void affecter()
    {
        base.affecter();

        //on modifie les valeurs globales
        if (!reset)
        {
            if (multiplication)
            {
                Level.sizePinceau = Level.sizePinceau * size;
            }
            else
            {
                Level.sizePinceau = Mathf.Max(1, Level.sizePinceau + size);
            }
        }
        else
        {
            Level.sizePinceau = 1;
        }
    }
}
