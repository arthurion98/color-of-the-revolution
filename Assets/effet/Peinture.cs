﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Peinture : Effet {

    long size;
    ColorFaction faction;
    Region region;
    bool reset;
    bool randomRegion;
    bool multiplication;
    bool allRegion;

    public Peinture(long size, ColorFaction faction, Region region, bool reset = false, bool randomRegion = false, bool multiplication = false, bool allRegion = false) : base()
    {
        this.size = size;
        this.region = region;
        this.faction = faction;
        this.reset = reset;
        this.randomRegion = randomRegion;
        this.multiplication = multiplication;
        this.allRegion = allRegion;
    }

    public override void affecter()
    {
        base.affecter();

        if (randomRegion) region = Level.randomRegion();
        List<Region> regionConcerned = new List<Region> { region };
        if (allRegion) regionConcerned = Level.regions;

        foreach(Region r in regionConcerned)
        {
            if (!reset)
            {
                if (multiplication)
                {
                    r.multiplyColor(faction, size);
                }
                else
                {
                    r.addColor(faction, size);
                }

            }
            else
            {
                r.resetColor(faction);
            }
        }

        
        
    }
}
