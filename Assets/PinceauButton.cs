﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PinceauButton : MonoBehaviour {

    public Button pinceau;

	// Use this for initialization
	void Start () {
        pinceau.onClick.AddListener(() =>
        {
            List<Region> RegionNonEmpty = new List<Region>();
            foreach(Region r in Level.regions)
            {
                if (r.getColors().Contains(Level.ownColor) && r.getOwnDominance() != r.getMaximumTerritory()) //si il y a déjà de la couleur de notre faction et on ne controle pas déjà tout, on rajoute la région à la liste
                {
                    
                    RegionNonEmpty.Add(r);
                }
            }

            if(RegionNonEmpty.Count == 0) //On a pas trouvé de région avec notre couleur, on choisit une région aléatoire où peindre
            {
                int number = Random.Range(0, Level.regions.Count);
                Level.regions[number].addColor(Level.ownColor, Level.sizePinceau);
            }
            else
            {
                int number = Random.Range(0, RegionNonEmpty.Count);
                RegionNonEmpty[number].addColor(Level.ownColor, Level.sizePinceau);
            }
        });
	}

    public void draw()
    {
        List<Region> RegionNonEmpty = new List<Region>();
        foreach (Region r in Level.regions)
        {
            if (r.getColors().Contains(Level.ownColor) && r.getOwnDominance() != r.getMaximumTerritory()) //si il y a déjà de la couleur de notre faction et on ne controle pas déjà tout, on rajoute la région à la liste
            {

                RegionNonEmpty.Add(r);
            }
        }

        if (RegionNonEmpty.Count == 0) //On a pas trouvé de région avec notre couleur, on choisit une région aléatoire où peindre
        {
            int number = Random.Range(0, Level.regions.Count);
            Level.regions[number].addColor(Level.ownColor, Level.sizePinceau);
        }
        else
        {
            int number = Random.Range(0, RegionNonEmpty.Count);
            RegionNonEmpty[number].addColor(Level.ownColor, Level.sizePinceau);
        }
    }
}
