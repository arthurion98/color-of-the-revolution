﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Event {

    public enum ButtonType { Left, Right };

    public int numero;
    public string description; //Le texte qui sera affiché
    public string button1, button2; //Le texte affiché sur chaque bouton
    public List<Effet> leftEvent, rightEvent; //Série de changement effectué selon l'appui du bouton gauche ou droite
    public bool uniqueChoice; //détermine si il n'y a qu'un bouton dispo (PAR DEFAUT CE SERA LE BOUTON GAUCHE)

    public Event(int numero, string description, string button1, string button2, List<Effet> leftEvent, List<Effet> rightEvent, bool uniqueChoice = false)
    {
        this.numero = numero;
        this.description = description;
        this.button1 = button1;
        this.button2 = button2;
        this.leftEvent = leftEvent;
        this.rightEvent = rightEvent;
        this.uniqueChoice = uniqueChoice;
    }

    public void activation(ButtonType type)
    {
        if(type == ButtonType.Left)
        {
            foreach(Effet e in leftEvent)
            {
                e.affecter();
            }
        }
        else
        {
            foreach (Effet e in rightEvent)
            {
                e.affecter();
            }
        }
    }
}
