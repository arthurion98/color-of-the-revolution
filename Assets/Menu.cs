﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Menu : MonoBehaviour {

    public static Color32 colorFaction;
    public static string factionName = "MyFaction";

    byte R = 0, G = 0, B = 0;

    public GameObject MenuPrincipal;
    public GameObject Game;

    //Menu principal
    public Button start;
    public Button colors;

    //Menu colors
    public GameObject colorMenu;
    public InputField newName;
    public Image color;
    public Slider left, center, right;
    public Button back;

	// Use this for initialization
	void Start () {

        start.onClick.AddListener(() =>
        {
            Level.initializer_static.initialize();

            MenuPrincipal.SetActive(false);
            Game.SetActive(true);
            Level.inGame = true;
        });

        colors.onClick.AddListener(() =>
        {
            start.gameObject.SetActive(false);
            colors.gameObject.SetActive(false);

            colorMenu.SetActive(true);
        });

        back.onClick.AddListener(() =>
        {
            Level.initializer_static.initialize(true);

            colorMenu.SetActive(false);

            /*start.gameObject.SetActive(true);
            colors.gameObject.SetActive(true);*/
            MenuPrincipal.SetActive(false);
            Game.SetActive(true);
            Level.inGame = true;
        });
	}
	
	// Update is called once per frame
	void Update () {
        if (colorMenu.activeSelf)
        {
            R = (byte)left.value;
            G = (byte)center.value;
            B = (byte)right.value;

            colorFaction = new Color32(R, G, B, 255);
            color.color = colorFaction;

            factionName = newName.text;
        }
	}
}
