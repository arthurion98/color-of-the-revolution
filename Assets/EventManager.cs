﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EventManager : MonoBehaviour {

    public static LinkedList<Event> eventList = new LinkedList<Event>(); //contient la liste des évenements en attente (agit comme une pile)

    public Text nombreEnAttente;
    private static int nbr;
    public Button listeEnAttente;
    public GameObject fenetreEvent;
    public Text description;
    public Button left;
    public Button right;
    public Text leftText;
    public Text rightText;
    public Image TimerSlice;

	// Use this for initialization
	void Start () {
        listeEnAttente.onClick.AddListener(() =>
        {
            if (!interface_region.showing_info && !fenetreEvent.activeSelf && eventList.Count != 0)
            {
                Level.chronoEventDelete = 0; //On remet le compteur à zéro
                fenetreEvent.SetActive(true);

                Event e = eventList.First.Value;
                removeEvent();

                if (e.uniqueChoice) //on désactive le bouton droit si jamais uniqueChoice est activé
                {
                    right.gameObject.SetActive(false);
                }
                else
                {
                    right.gameObject.SetActive(true);
                }

                description.text = e.description;
                leftText.text = e.button1;
                rightText.text = e.button2;

                left.onClick.RemoveAllListeners(); //au cas où
                left.onClick.AddListener(() =>
                {
                    e.activation(Event.ButtonType.Left);

                    fenetreEvent.SetActive(false);
                });

                right.onClick.RemoveAllListeners();
                right.onClick.AddListener(() =>
                {
                    e.activation(Event.ButtonType.Right);

                    fenetreEvent.SetActive(false);
                });
            }


        });
	}

    public void openNewEvent()
    {
        if (!interface_region.showing_info && !fenetreEvent.activeSelf && eventList.Count != 0)
        {
            Level.chronoEventDelete = 0; //On remet le compteur à zéro
            fenetreEvent.SetActive(true);

            Event e = eventList.First.Value;
            removeEvent();

            if (e.uniqueChoice) //on désactive le bouton droit si jamais uniqueChoice est activé
            {
                right.gameObject.SetActive(false);
            }
            else
            {
                right.gameObject.SetActive(true);
            }

            description.text = e.description;
            leftText.text = e.button1;
            rightText.text = e.button2;

            left.onClick.RemoveAllListeners(); //au cas où
            left.onClick.AddListener(() =>
            {
                e.activation(Event.ButtonType.Left);

                fenetreEvent.SetActive(false);
            });

            right.onClick.RemoveAllListeners();
            right.onClick.AddListener(() =>
            {
                e.activation(Event.ButtonType.Right);

                fenetreEvent.SetActive(false);
            });
        }
    }
	
	// Update is called once per frame
	void Update () {
        nombreEnAttente.text = nbr > 99 ? "99+" : nbr.ToString();
        TimerSlice.fillAmount = Level.chronoEventDelete / Level.cycle_time_delete;
    }

    public static void addEvent(Event e)
    {
        eventList.AddLast(e);
        nbr += 1;
        
    }

    public static void removeEvent()
    {
        eventList.RemoveFirst();
        nbr -= 1;
    }
}
